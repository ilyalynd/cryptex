# CryptEx change log

## Version 1.0.0 August 03, 2021

- New: Added Index page template
- New: Added TT Firs Neue, TT Commons and Audiowide fonts
- New: Added jQuery
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init July 29, 2021

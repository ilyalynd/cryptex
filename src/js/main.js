(function ($) {
  'use strict'

  // плавная прокрутка к якорю
  $('.hash').on('click', function (event) {
    event.preventDefault()

    let sectionId = $(this).attr('href'),
        pageTop = $(sectionId).offset().top + 48

    $('html, body').animate({ scrollTop: pageTop }, 600)
  })

  // вкладки
  let tab = $('.rewards-dropdown .rewards-dropdown-list__item'),
      nav = $('.rewards-dropdown .rewards-dropdown-nav a')

  nav.on('click', function (event) {
    event.preventDefault()

    nav.removeClass('active')
    tab.hide().filter($(this).attr('href')).show()
    $(this).addClass('active')
  })

  // модификатор ссылок
  if (location.search !== '') {
    $('a[href*="//cryptexlock.me"]').attr('href', function(i, h) {
      return h + location.search
    })
  }
}(jQuery))
